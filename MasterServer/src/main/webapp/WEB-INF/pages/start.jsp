<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <link href="bootstrap/css/bootstrap.css" rel="stylesheet" media="screen">
</head>
<body>
    <div style="margin: 20px auto 0 auto; width: 400px">
        <form:form method="POST" action="/master/fire">
            <table>
                <tr>
                    <td><form:label path="iterations">Iterations</form:label></td>
                    <td><form:input path="iterations" /></td>
                </tr>
                <tr>
                    <td><form:label path="islandsCount">Islands count</form:label></td>
                    <td><form:input path="islandsCount" /></td>
                </tr>
                <tr>
                    <td><form:label path="islandsIndividuals">Count of individuals on the island</form:label></td>
                    <td><form:input path="islandsIndividuals" /></td>
                </tr>
                <tr>
                    <td><form:label path="objectsCount">Objects count</form:label></td>
                    <td><form:input path="objectsCount" /></td>
                </tr>
                <tr>
                    <td><form:label path="islandsPerRequest">Islands per request</form:label></td>
                    <td><form:input path="islandsPerRequest" /></td>
                </tr>
                <tr>
                    <td><form:label path="capacity">Capacity</form:label></td>
                    <td><form:input path="capacity" /></td>
                </tr>
                <tr>
                    <td><form:label path="maxRandom">Max random masses and values</form:label></td>
                    <td><form:input path="maxRandom" /></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <input type="submit" class="btn btn-primary" value="Odpal"/>
                    </td>
                </tr>
            </table>
        </form:form>
    </div>
</body>
</html>