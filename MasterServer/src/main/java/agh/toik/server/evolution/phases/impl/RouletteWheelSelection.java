package agh.toik.server.evolution.phases.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import agh.toik.common.Individual;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import agh.toik.server.evolution.phases.Selection;

public class RouletteWheelSelection implements Selection {

	private final static Random random = new Random();
	private List<FitnessContainer> fitnessContainers = new ArrayList<FitnessContainer>();
	private Double fitnessSum = 0.0;

	private boolean isFitnessSumGreaterThanZero() {
		return fitnessSum > 0.0;
	}

	private class FitnessContainer implements Comparable<FitnessContainer> {
		public Individual individual;
		public Double fitness;
		public Double scaledFitness;
		public Double cumulativeScaledFitness;

		public FitnessContainer(Individual individual, Double fitness) {
			this.individual = individual;
			this.fitness = fitness;
		}

		@Override
		public int compareTo(FitnessContainer o) {
			return fitness.compareTo(o.fitness);
		}

		@Override
		public String toString() {
			return "FitnessContainer [individual=" + individual + ", fitness=" + fitness + ", scaledFitness="
					+ scaledFitness + ", cumulativeScaledFitness=" + cumulativeScaledFitness + "]";
		}
	}

	public RouletteWheelSelection(List<Individual> individuals, List<Double> fitnesses) {
		initializeSelection(individuals, fitnesses);
	}

	private void initializeSelection(List<Individual> individuals, List<Double> fitnesses) {
		if (individuals.size() != fitnesses.size()) {
			throw new RuntimeException(String.format("Fitnessess size %d is other than individuals size %d",
					fitnesses.size(), individuals.size()));
		}
		initializeFitnessContainers(individuals, fitnesses);
		calculateFitnessSum();
		Collections.sort(fitnessContainers); // sort by fitness, ascending
		calculateScaledFitnesses();
		calculateCumulativeScaledFitnesses();
	}

	private void initializeFitnessContainers(List<Individual> individuals, List<Double> fitnesses) {
		for (int i = 0; i < individuals.size(); i++) {
			fitnessContainers.add(new FitnessContainer(individuals.get(i), fitnesses.get(i)));
		}
	}

	private void calculateFitnessSum() {
		fitnessSum = 0.0;
		for (FitnessContainer fitnessContainer : fitnessContainers) {
			fitnessSum += fitnessContainer.fitness;
		}
	}

	private void calculateScaledFitnesses() {
		for (FitnessContainer fitnessContainer : fitnessContainers) {
			fitnessContainer.scaledFitness = isFitnessSumGreaterThanZero() ? (fitnessContainer.fitness / fitnessSum)
					: 1.0 / fitnessContainers.size();
		}
	}

	private void calculateCumulativeScaledFitnesses() {
		double cumulativeFitness = 0.0f;
		for (int i = 0; i < fitnessContainers.size(); ++i) {
			cumulativeFitness += fitnessContainers.get(i).scaledFitness;
			fitnessContainers.get(i).cumulativeScaledFitness = cumulativeFitness;
		}
	}

	private Individual selectIndividual() {
		Double randomNumber = random.nextDouble();
		for (FitnessContainer fitnessContainer : fitnessContainers) {
			if (fitnessContainer.cumulativeScaledFitness > randomNumber) {
				return fitnessContainer.individual;
			}
		}
		return null;
	}

	@Override
	public Pair<Individual, Individual> selectParents() {
		return new ImmutablePair<Individual, Individual>(selectIndividual(), selectIndividual());
	}
}
