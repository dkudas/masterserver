package agh.toik.server.evolution.phases.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import agh.toik.server.evolution.phases.NeighboringIslandsSelection;

public class RingNeighboringIslandsSelection implements NeighboringIslandsSelection {

	@Override
	public List<Pair<CountingIslandEntity, CountingIslandResult>> getNeighboringIslands(
			Map<Long, CountingIslandEntity> islands, Map<Long, CountingIslandResult> islandsFitnesses, Long islandId) {
		if (islands == null) {
			throw new NullPointerException("'islands' argument must not be null");
		}
		if (islandsFitnesses == null) {
			throw new NullPointerException("'islandsFitnesses' argument must not be null");
		}
		
		List<Pair<CountingIslandEntity, CountingIslandResult>> results = new ArrayList<Pair<CountingIslandEntity, CountingIslandResult>>();
		
		List<Long> ids = new ArrayList<Long>(islands.keySet());
		Collections.sort(ids);
		int islandIdx = ids.indexOf(islandId);
		if (islandIdx == -1) {
			throw new RuntimeException("Island with id " + islandId + " doesn't exist");
		}
		
		// if there's only one island, there can't be any neighbors
		if (islands.size() > 1) {
			Long leftNeighborId = ids.get(islandIdx-1 >= 0 ? islandIdx-1 : ids.size()-1);
			results.add(new ImmutablePair<CountingIslandEntity, CountingIslandResult>(
					islands.get(leftNeighborId), islandsFitnesses.get(leftNeighborId)));
		}
		
		// if there are only two islands, there can't be two neighbors
		if (islands.size() > 2) {
			Long rightNeighborId = ids.get(islandIdx+1 < ids.size() ? islandIdx+1 : 0);
			results.add(new ImmutablePair<CountingIslandEntity, CountingIslandResult>(
					islands.get(rightNeighborId), islandsFitnesses.get(rightNeighborId)));
		}

		return results;
	}

}
