package agh.toik.server.evolution.phases.impl;

import java.util.Random;

import agh.toik.common.Individual;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import agh.toik.server.evolution.phases.CrossOver;

public class SinglePointCrossOver implements CrossOver {

	private final static Random random = new Random();

	@Override
	public Pair<Individual, Individual> crossOver(Individual firstParent, Individual secondParent) {
		if (firstParent.getLength() != secondParent.getLength()) {
			throw new RuntimeException("Cannot cross over two individuals with different number of genes");
		}
		int length = firstParent.getLength();
		boolean[] firstChildGenes = firstParent.getObjects();
		boolean[] secondChildGenes = secondParent.getObjects();
		int borderPoint = random.nextInt(length);

		swapGenesToBorderPoint(firstChildGenes, secondChildGenes, borderPoint);

		Individual firstChild = new Individual(firstChildGenes);
		Individual secondChild = new Individual(secondChildGenes);
		return new ImmutablePair<Individual, Individual>(firstChild, secondChild);
	}

	private void swapGenesToBorderPoint(boolean[] firstChildGenes, boolean[] secondChildGenes, int borderPoint) {
		for (int geneNumber = 0; geneNumber < borderPoint; geneNumber++) {
			swapGene(firstChildGenes, secondChildGenes, geneNumber);
		}
	}

	private void swapGene(boolean[] firstGenes, boolean[] secondGenes, int geneNumber) {
		boolean tmp = firstGenes[geneNumber];
		firstGenes[geneNumber] = secondGenes[geneNumber];
		secondGenes[geneNumber] = tmp;
	}
}
