package agh.toik.server.evolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import agh.toik.common.Individual;
import org.apache.commons.lang3.tuple.Pair;

import agh.toik.server.evolution.phases.CrossOver;
import agh.toik.server.evolution.phases.Migration;
import agh.toik.server.evolution.phases.Mutation;
import agh.toik.server.evolution.phases.NeighboringIslandsSelection;
import agh.toik.server.evolution.phases.Selection;
import agh.toik.server.evolution.phases.impl.ProbabilisticGenesInversion;
import agh.toik.server.evolution.phases.impl.RingNeighboringIslandsSelection;
import agh.toik.server.evolution.phases.impl.RouletteWheelSelection;
import agh.toik.server.evolution.phases.impl.SinglePointCrossOver;
import agh.toik.server.evolution.phases.impl.RandomMigration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EvolutionProcessor {

	private final static Random random = new Random();
	private final CrossOver crossOver = new SinglePointCrossOver();
	private final Mutation mutation = new ProbabilisticGenesInversion(0.1);
	private final NeighboringIslandsSelection neighboringIslandsSelection = new RingNeighboringIslandsSelection();
	private final Migration migration = new RandomMigration(0.1);

	private Double crossOverProbability = 0.5;
	private Double mutationProbability = 0.1;
	private boolean elitism = true;

    @Autowired
	public EvolutionProcessor(@Value("${crossOverProbability}") Double crossOverProbability,
                              @Value("${mutationProbability}") Double mutationProbability,
                              @Value("${elitism}") boolean elitism) {
		this.crossOverProbability = crossOverProbability;
		this.mutationProbability = mutationProbability;
		this.elitism = elitism;
	}

	/**
	 * Performs necessary selection/cross-over/mutation steps on the list of individuals and returns the list of
	 * individuals that should be used in the next iteration.
	 * 
	 * Package private access so that we can test this method, but we can't use it outside this package.
	 */
	List<Individual> getIndividualsForNextIteration(List<Individual> currentIndividuals, List<Double> fitnesses) {
		List<Individual> newPopulation = new ArrayList<Individual>();
		if (elitism) {
			// the best individual shouldn't be lost with this option
			Double maxFitness = Collections.max(fitnesses);
			int maxFitnessIndex = fitnesses.indexOf(maxFitness);
			Individual bestIndividual = currentIndividuals.get(maxFitnessIndex);
			newPopulation.add(bestIndividual);
		}
		Selection selection = new RouletteWheelSelection(currentIndividuals, fitnesses);
		while (newPopulation.size() < currentIndividuals.size()) {
			Pair<Individual, Individual> parents = selection.selectParents();
			Individual firstIndividual = parents.getLeft();
			Individual secondIndividual = parents.getRight();
			if (random.nextDouble() < crossOverProbability) {
				Pair<Individual, Individual> crossOvered = crossOver.crossOver(firstIndividual, secondIndividual);
				firstIndividual = crossOvered.getLeft();
				secondIndividual = crossOvered.getRight();
			}
			if (random.nextDouble() < mutationProbability) {
				firstIndividual = mutation.mutate(firstIndividual);
			}
			if (random.nextDouble() < mutationProbability) {
				secondIndividual = mutation.mutate(secondIndividual);
			}
			newPopulation.add(firstIndividual);
			newPopulation.add(secondIndividual);
		}
		return newPopulation;
	}

	public Map<Long, CountingIslandEntity> getCountingIslandEntitiesForNextIteration(
			Map<Long, CountingIslandEntity> islands, Map<Long, CountingIslandResult> islandsFitnesses) {
		if (islands == null) {
			throw new NullPointerException("'islands' argument must not be null");
		}
		if (islandsFitnesses == null) {
			throw new NullPointerException("'islandsFitnesses' argument must not be null");
		}
		
		// migration
		for (Long islandId : islands.keySet()) {
			List<Pair<CountingIslandEntity, CountingIslandResult>> neighboringIslands = 
					neighboringIslandsSelection.getNeighboringIslands(islands, islandsFitnesses, islandId);			
			for (Pair<CountingIslandEntity, CountingIslandResult> island : neighboringIslands) {
				migration.migrate(islands.get(islandId), islandsFitnesses.get(islandId), island.getLeft(), island.getRight());
			}
		}
		
		// proper evolution (crossover, mutation, etc.)
		Map<Long, CountingIslandEntity> newIslands = new HashMap<Long, CountingIslandEntity>();
		for (Long islandId : islands.keySet()) {
			List<Individual> newIslandPopulation = 
					getIndividualsForNextIteration(islands.get(islandId).getIndividuals(), islandsFitnesses.get(islandId).getFitnesses());
			newIslands.put(islandId, new CountingIslandEntity(islandId, newIslandPopulation));
		}

		return newIslands;
	}
}