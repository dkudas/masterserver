package agh.toik.server.evolution;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import agh.toik.common.Individual;
import agh.toik.common.communication.CountingEntity;
import agh.toik.common.communication.CountingResult;
import agh.toik.server.executor.Configuration;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class FitnessCalculator {

    private Object lock = new Object();

    private Map<Long, CountingEntity> countingEntities;
    private Set<Long> waitingTasks;
    private Map<Long, Long> sentTime;
    private List<CountingResult> countingResults;
    private long id = 0;
    /**
	 * Calculates fitness function values for the given list of individuals using volunteers' computers with the help of
	 * slave server nodes.
	 */
	public Map<Long, CountingIslandResult> calculateFitness(Map<Long, CountingIslandEntity> countingIslandEntities, List<Double> masses, List<Double> values,
                                                            double capacity, Configuration configuration) {

        Map<Long, CountingEntity> countingEntitiesTemp = countingEntitiesTemp(countingIslandEntities, masses, values, capacity, configuration);

        synchronized (lock) {
            countingEntities = countingEntitiesTemp;
            waitingTasks = countingEntitiesTemp.keySet();
            sentTime = new HashMap<Long, Long>();
            countingResults = new ArrayList<CountingResult>();
        }

        List<CountingResult> countingResultsTemp;
        synchronized (lock) {
            if (waitingTasks.size() > 0) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            countingResultsTemp = countingResults;
            countingResults = null;
            countingEntities = null;
        }

        Map<Long, CountingIslandResult> fitnesses = new HashMap<Long, CountingIslandResult>();
        for (CountingResult countingResult : countingResultsTemp) {
            for (CountingIslandResult countingIslandResult : countingResult.getCountingIslandResults()) {
                fitnesses.put(countingIslandResult.getId(), countingIslandResult);
            }
        }
        return fitnesses;
	}

    public CountingEntity getTasksForSlave() {
        synchronized (lock) {
            if (waitingTasks != null && waitingTasks.size() > 0) {
                for (Long id : waitingTasks) {
                    if (sentTime.get(id) == null || (System.currentTimeMillis() - sentTime.get(id)) > 1000 * 60) {
                        sentTime.put(id, System.currentTimeMillis());
                        return countingEntities.get(id);

                    }
                }
                return emptyEntity();
            } else {
                return emptyEntity();
            }
        }
    }

    private CountingEntity emptyEntity() {
        List<CountingIslandEntity> emptyCountingIslandEntities = new ArrayList<CountingIslandEntity>();
        List<Double> empty = new ArrayList<Double>();
        return new CountingEntity(-1, 0.0, emptyCountingIslandEntities, empty, empty);
    }

    public void resultsFromSlave(CountingResult countingResult) {
        synchronized (lock) {
            if (countingResults != null) {
                if (waitingTasks.contains(countingResult.getId()))
                    waitingTasks.remove(countingResult.getId());
                    countingResults.add(countingResult);
                    if (waitingTasks.isEmpty()) {
                        lock.notify();
                    }
            }
        }

    }

    private Map<Long, CountingEntity> countingEntitiesTemp(Map<Long, CountingIslandEntity> countingIslandEntities, List<Double> masses, List<Double> values,
                                                      double capacity, Configuration configuration) {
        Map<Long, CountingEntity> countingEntitiesTemp = new HashMap<Long, CountingEntity>();
        List<CountingIslandEntity> countingIslands = new ArrayList<CountingIslandEntity>(countingIslandEntities.values());
        while (countingIslands.size() > 0) {
            int i = 0;
            List<CountingIslandEntity> partOfCountingIslandEntities = new ArrayList<CountingIslandEntity>();
            while (i < configuration.getIslandsPerRequest() && countingIslands.size() > 0) {
                partOfCountingIslandEntities.add(countingIslands.remove(0));
                ++i;
            }
            CountingEntity countingEntity = new CountingEntity(id, capacity, partOfCountingIslandEntities, masses, values);
            if (partOfCountingIslandEntities.size() > 0) {
                countingEntitiesTemp.put(id++, countingEntity);
            }
        }

        return countingEntitiesTemp;
    }
}
