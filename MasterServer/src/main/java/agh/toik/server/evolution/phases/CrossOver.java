package agh.toik.server.evolution.phases;

import agh.toik.common.Individual;
import org.apache.commons.lang3.tuple.Pair;


public interface CrossOver {
	public Pair<Individual, Individual> crossOver(Individual firstParent, Individual secondParent);
}
