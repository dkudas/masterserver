package agh.toik.server.evolution.phases;

import java.util.List;
import java.util.Map;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import org.apache.commons.lang3.tuple.Pair;

public interface NeighboringIslandsSelection {
	/*
	 * For the given island specified by 'islandId' returns a list of its neighboring islands and their fitnesses
	 */
	List<Pair<CountingIslandEntity, CountingIslandResult>> getNeighboringIslands(Map<Long, CountingIslandEntity> islands,
			Map<Long, CountingIslandResult> islandsFitnesses, Long islandId);
}
