package agh.toik.server.evolution.phases;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;

public interface Migration {
	/*
	 * Performs in-place migrations (the arguments are in-out arguments)
	 */
	void migrate(CountingIslandEntity island1, CountingIslandResult fitnesses1,
			CountingIslandEntity island2, CountingIslandResult fitnesses2);
}
