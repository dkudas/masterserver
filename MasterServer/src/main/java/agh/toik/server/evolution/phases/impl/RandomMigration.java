package agh.toik.server.evolution.phases.impl;

import java.util.List;
import java.util.Random;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import agh.toik.common.Individual;
import agh.toik.server.evolution.phases.Migration;

public class RandomMigration implements Migration {
	
	private final static Random random = new Random();
	
	private final double migrationProbability;
	
	public RandomMigration(double migrationProbability) {
		this.migrationProbability = migrationProbability;
	}

	@Override
	public void migrate(CountingIslandEntity island1, CountingIslandResult fitnesses1, CountingIslandEntity island2,
			CountingIslandResult fitnesses2) {
		List<Individual> individuals1 = island1.getIndividuals();
		List<Individual> individuals2 = island2.getIndividuals();
		int limit = Math.min(individuals1.size(), individuals2.size());
		for (int i = 0; i < limit; i++) {
			if (random.nextDouble() < migrationProbability) {
				migrateIndividual(individuals1, individuals2, i);
			}
		}
	}
	
	private void migrateIndividual(List<Individual> individuals1, List<Individual> individuals2, int individualIdx) {
		Individual tmp = individuals1.get(individualIdx);
		individuals1.set(individualIdx, individuals2.get(individualIdx));
		individuals2.set(individualIdx, tmp);
	}

}
