package agh.toik.server.evolution.phases;


import agh.toik.common.Individual;

public interface Mutation {
	public Individual mutate(Individual individual);
}
