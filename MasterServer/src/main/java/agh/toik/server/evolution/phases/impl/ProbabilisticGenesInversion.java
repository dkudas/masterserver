package agh.toik.server.evolution.phases.impl;

import agh.toik.common.Individual;
import agh.toik.server.evolution.phases.Mutation;

import java.util.Random;

public class ProbabilisticGenesInversion implements Mutation {

	private final static Random random = new Random();

	private double probabilityOfGeneMutation;

	public ProbabilisticGenesInversion(double probabilityOfGeneMutation) {
		super();
		this.probabilityOfGeneMutation = probabilityOfGeneMutation;
	}

	@Override
	public Individual mutate(Individual individual) {
		boolean[] genes = individual.getObjects();
		for (int i = 0; i < genes.length; ++i) {
			if (random.nextDouble() < probabilityOfGeneMutation) {
				genes[i] = !genes[i];
			}
		}
		return new Individual(genes);
	}

}
