package agh.toik.server.evolution;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import agh.toik.server.executor.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class EvolutionManager {

	private FitnessCalculator fitnessCalculator;
	private EvolutionProcessor evolutionProcessor;

    @Autowired
	public EvolutionManager(FitnessCalculator fitnessCalculator, EvolutionProcessor evolutionProcessor) {
		this.fitnessCalculator = fitnessCalculator;
		this.evolutionProcessor = evolutionProcessor;
	}

	public Map<Long, CountingIslandEntity> performEvolution(Map<Long, CountingIslandEntity> initialCountingIslandEntities, List<Double> masses,
                                                            List<Double> values, double capacity, int iterations, Configuration configuration) {

        Map<Long, CountingIslandEntity> currentCountingIslandEntities = initialCountingIslandEntities;
		for (int i = 0; i < iterations; ++i) {

			Map<Long, CountingIslandResult> fitnesses = fitnessCalculator.calculateFitness(currentCountingIslandEntities, masses, values, capacity, configuration);
            currentCountingIslandEntities = evolutionProcessor.getCountingIslandEntitiesForNextIteration(currentCountingIslandEntities, fitnesses);
		}
		return currentCountingIslandEntities;
	}
}
