package agh.toik.server.executor;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.Individual;
import agh.toik.server.evolution.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.*;

/**
 * Created by Adiki on 2014-05-12.
 */

@Component
public class EvolutionExecutor {

    @Autowired
    private EvolutionManager evolutionManager;

    private static final Logger logger = Logger.getLogger(EvolutionExecutor.class);
    private volatile boolean done = true;
    private Map<Long, CountingIslandEntity> countingIslandEntities;
    private Properties props = new Properties();

    public void execute(final Configuration configuration) {
        done = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                logger.info("Start execution");
                long startTime = System.currentTimeMillis();
                loadProps();

                Double capacity = configuration.getCapacity();

                Random random = new Random();
                List<Double> masses = new ArrayList<Double>();
                List<Double> values = new ArrayList<Double>();
                for (int i = 0; i < configuration.getObjectsCount(); ++i) {
                    masses.add(random.nextDouble() * configuration.getMaxRandom());
                    values.add(random.nextDouble() * configuration.getMaxRandom());
                }

                Map<Long, CountingIslandEntity> initialCountingIslandEntities = prepareCountingIslandEntities(configuration);
                countingIslandEntities = evolutionManager.performEvolution(initialCountingIslandEntities, masses, values, capacity, configuration.getIterations(), configuration);
                logger.debug("Execution time: " + (System.currentTimeMillis() - startTime));
                done = true;
            }
        }).start();
    }

    public boolean isDone() {
        return done;
    }

    public Map<Long, CountingIslandEntity> countingIslandResults() {
        if(done) {
            return countingIslandEntities;
        }
        return null;
    }

    private void loadProps() {
        Resource resource = new ClassPathResource("evolution.properties");
        try {
            props.load(resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<Long, CountingIslandEntity> prepareCountingIslandEntities(Configuration configuration) {
        Random random = new Random();
        Map<Long, CountingIslandEntity> initialCountingIslandEntities = new HashMap<Long, CountingIslandEntity>();
        for (long i = 0; i < configuration.getIslandsCount(); ++i) {
            List<Individual> individuals = new ArrayList<Individual>();
            for (int j = 0; j < configuration.getIslandsIndividuals(); ++j) {

                boolean[] objects = new boolean[configuration.getObjectsCount()];
                for (int k = 0; k < configuration.getObjectsCount(); ++k) {
                    objects[k] = random.nextBoolean();
                }
                Individual individual = new Individual(objects);
                individuals.add(individual);
            }
            CountingIslandEntity countingIslandEntity = new CountingIslandEntity(i, individuals);
            initialCountingIslandEntities.put(i, countingIslandEntity);
        }

        return initialCountingIslandEntities;
    }
}
