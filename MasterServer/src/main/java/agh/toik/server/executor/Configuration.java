package agh.toik.server.executor;

/**
 * Created by Adiki on 2014-05-26.
 */
public class Configuration {
    private Integer iterations;
    private Integer islandsCount;
    private Integer islandsIndividuals;
    private Integer objectsCount;
    private Integer islandsPerRequest;
    private Double capacity;
    private Double maxRandom;

    public Double getCapacity() {
        return capacity;
    }

    public void setCapacity(Double capacity) {
        this.capacity = capacity;
    }

    public Double getMaxRandom() {
        return maxRandom;
    }

    public void setMaxRandom(Double maxRandom) {
        this.maxRandom = maxRandom;
    }

    public Integer getIterations() {
        return iterations;
    }

    public void setIterations(Integer iterations) {
        this.iterations = iterations;
    }

    public Integer getIslandsCount() {
        return islandsCount;
    }

    public void setIslandsCount(Integer islandsCount) {
        this.islandsCount = islandsCount;
    }

    public Integer getIslandsIndividuals() {
        return islandsIndividuals;
    }

    public void setIslandsIndividuals(Integer islandsIndividuals) {
        this.islandsIndividuals = islandsIndividuals;
    }

    public Integer getObjectsCount() {
        return objectsCount;
    }

    public void setObjectsCount(Integer objectsCount) {
        this.objectsCount = objectsCount;
    }

    public Integer getIslandsPerRequest() {
        return islandsPerRequest;
    }

    public void setIslandsPerRequest(Integer islandsPerRequest) {
        this.islandsPerRequest = islandsPerRequest;
    }
}
