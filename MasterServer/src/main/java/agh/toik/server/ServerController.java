package agh.toik.server;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.Individual;
import agh.toik.common.communication.CountingEntity;
import agh.toik.common.communication.CountingResult;
import agh.toik.server.evolution.FitnessCalculator;
import agh.toik.server.executor.Configuration;
import agh.toik.server.executor.EvolutionExecutor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;

// 30 - 50 osobnikow na wyspie
// wysp 1000
// migracje wedlug ringu
// komponent do zarzadzajacy migracjami
/**
 * Created by DAMIAN on 02.05.14.
 */
@Controller
public class ServerController {
    private static final Logger logger = Logger.getLogger(ServerController.class);
    @Autowired
    private EvolutionExecutor evolutionExecutor;
    @Autowired
    private FitnessCalculator fitnessCalculator;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView home() {
        if (evolutionExecutor.isDone()) {
            return new ModelAndView("start", "command", new Configuration());
        }
        return new ModelAndView("redirect:/check");
    }

    @RequestMapping(value = "fire", method = RequestMethod.POST)
    public String fire(@ModelAttribute("SpringWeb")Configuration configuration) {
        evolutionExecutor.execute(configuration);

        return "redirect:/check";
    }

    @RequestMapping(value = "check", method = RequestMethod.GET)
    @ResponseBody
    public String check() {
        if (evolutionExecutor.isDone() && evolutionExecutor.countingIslandResults() != null) {
            String result = "";
            for(CountingIslandEntity countingIslandEntity : evolutionExecutor.countingIslandResults().values()) {
               result += countingIslandEntity.getId() + " " + countingIslandEntity.getIndividuals().toString() + "<BR>";
            }
            return result;
        }
        return "Counting";
    }

    @RequestMapping(value = "tasksForSlave", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String tasksForSlave() {
        CountingEntity countingEntity = fitnessCalculator.getTasksForSlave();
        return countingEntity.toJson();
    }

    @RequestMapping(value = "resultsFromSlave", method = RequestMethod.POST)
    @ResponseBody
    public String sendResult(@RequestBody String countingResultJSON){
        try {
            logger.debug(countingResultJSON);
            CountingResult countingResult = CountingResult.fromJson(countingResultJSON);
            fitnessCalculator.resultsFromSlave(countingResult);
        } catch (Exception e) {
            logger.trace("Excpetion", e);
        }
        return "Thank you";
    }
}
