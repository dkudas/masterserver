package agh.toik.server.evolution.phases.impl;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.BeforeClass;
import org.junit.Test;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import agh.toik.server.evolution.phases.NeighboringIslandsSelection;

public class RingNeighboringIslandsSelectionTest {
	
	private static NeighboringIslandsSelection selection = new RingNeighboringIslandsSelection();
	
	private static Map<Long, agh.toik.common.CountingIslandEntity> fourIslands = new HashMap<Long, CountingIslandEntity>();
	private static Map<Long, CountingIslandResult> fourIslandsFitnesses = new HashMap<Long, CountingIslandResult>();
	
	private static Map<Long, agh.toik.common.CountingIslandEntity> twoIslands = new HashMap<Long, CountingIslandEntity>();
	private static Map<Long, CountingIslandResult> twoIslandsFitnesses = new HashMap<Long, CountingIslandResult>();
	
	private static Map<Long, agh.toik.common.CountingIslandEntity> oneIsland = new HashMap<Long, CountingIslandEntity>();
	private static Map<Long, CountingIslandResult> oneIslandFitnesses = new HashMap<Long, CountingIslandResult>();
	
	@BeforeClass
	public static void init() {
		Long fourIslandIds[] = {50L, 10L, 40L, 20L};
		for (Long id : fourIslandIds) {
			fourIslands.put(id, new CountingIslandEntity(id, null));
			fourIslandsFitnesses.put(id, new CountingIslandResult(id, null));
		}
		
		Long twoIslandsIds[] = {20L, 10L};
		for (Long id : twoIslandsIds) {
			twoIslands.put(id, new CountingIslandEntity(id, null));
			twoIslandsFitnesses.put(id, new CountingIslandResult(id, null));
		}
		
		oneIsland.put(10L, new CountingIslandEntity(10L, null));
		oneIslandFitnesses.put(10L, new CountingIslandResult(10L, null));
	}
	
	@Test
	public void islandInTheMiddleOfFour() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(fourIslands, fourIslandsFitnesses, 20L);
		assertEquals(2, res.size());
		
		Pair<CountingIslandEntity, CountingIslandResult> leftNeighbor = res.get(0);
		assertEquals(10L, leftNeighbor.getLeft().getId());
		assertEquals(10L, leftNeighbor.getRight().getId());
		
		Pair<CountingIslandEntity, CountingIslandResult> rightNeighbor = res.get(1);
		assertEquals(40L, rightNeighbor.getLeft().getId());
		assertEquals(40L, rightNeighbor.getRight().getId());
	}
	
	@Test
	public void firstIslandOfFour() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(fourIslands, fourIslandsFitnesses, 10L);
		assertEquals(2, res.size());
		
		Pair<CountingIslandEntity, CountingIslandResult> leftNeighbor = res.get(0);
		assertEquals(50L, leftNeighbor.getLeft().getId());
		assertEquals(50L, leftNeighbor.getRight().getId());
		
		Pair<CountingIslandEntity, CountingIslandResult> rightNeighbor = res.get(1);
		assertEquals(20L, rightNeighbor.getLeft().getId());
		assertEquals(20L, rightNeighbor.getRight().getId());
	}
	
	@Test
	public void lastIslandOfFour() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(fourIslands, fourIslandsFitnesses, 50L);
		assertEquals(2, res.size());
		
		Pair<CountingIslandEntity, CountingIslandResult> leftNeighbor = res.get(0);
		assertEquals(40L, leftNeighbor.getLeft().getId());
		assertEquals(40L, leftNeighbor.getRight().getId());
		
		Pair<CountingIslandEntity, CountingIslandResult> rightNeighbor = res.get(1);
		assertEquals(10L, rightNeighbor.getLeft().getId());
		assertEquals(10L, rightNeighbor.getRight().getId());
	}
	
	@Test
	public void firstIslandOfTwo() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(twoIslands, twoIslandsFitnesses, 10L);
		assertEquals(1, res.size());
		
		Pair<CountingIslandEntity, CountingIslandResult> neighbor = res.get(0);
		assertEquals(20L, neighbor.getLeft().getId());
		assertEquals(20L, neighbor.getRight().getId());
	}
	
	@Test
	public void secondIslandOfTwo() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(twoIslands, twoIslandsFitnesses, 20L);
		assertEquals(1, res.size());
		
		Pair<CountingIslandEntity, CountingIslandResult> neighbor = res.get(0);
		assertEquals(10L, neighbor.getLeft().getId());
		assertEquals(10L, neighbor.getRight().getId());
	}
	
	@Test
	public void solitaryIsland() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(oneIsland, oneIslandFitnesses, 10L);
		assertEquals(0, res.size());
	}
	
	@Test(expected = RuntimeException.class)
	public void wrongIslandId() {
		List<Pair<CountingIslandEntity, CountingIslandResult>> res = selection.getNeighboringIslands(oneIsland, oneIslandFitnesses, -1L);
		assertEquals(0, res.size());
	}
	
	@Test(expected = NullPointerException.class)
	public void nullIslands() {
		selection.getNeighboringIslands(null, oneIslandFitnesses, 10L);
	}
	
	@Test(expected = NullPointerException.class)
	public void nullIslandsFitnesses() {
		selection.getNeighboringIslands(oneIsland, null, 10L);
	}

}
