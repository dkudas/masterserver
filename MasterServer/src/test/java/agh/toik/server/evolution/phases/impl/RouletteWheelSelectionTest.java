package agh.toik.server.evolution.phases.impl;

import java.util.Arrays;

import agh.toik.common.Individual;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Assert;
import org.junit.Test;


public class RouletteWheelSelectionTest {
	@Test
	public void greaterThanZeroFitnessAreSelectedBeforeZeroFitness() {
		Individual i0 = new Individual(new boolean[] { false, false });
		Individual i1 = new Individual(new boolean[] { false, false });
		Individual i2 = new Individual(new boolean[] { false, true });
		Individual i3 = new Individual(new boolean[] { true, true });
		Double[] fitnesses = new Double[] { 0.0, 0.0, 1.0, 2.0 };
		RouletteWheelSelection selection = new RouletteWheelSelection(Arrays.asList(i0, i1, i2, i3),
				Arrays.asList(fitnesses));
		Pair<Individual, Individual> selectParents = selection.selectParents();
		Assert.assertTrue(selectParents.getLeft().equals(i3) || selectParents.getLeft().equals(i2));
		Assert.assertTrue(selectParents.getRight().equals(i3) || selectParents.getRight().equals(i2));
	}
}
