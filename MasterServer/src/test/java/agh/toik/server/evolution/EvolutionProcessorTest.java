package agh.toik.server.evolution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import agh.toik.common.Individual;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class EvolutionProcessorTest {

	private static final Logger logger = Logger.getLogger(EvolutionProcessorTest.class);

	private List<Double> calculateFitnessesFromGenesAsBinaryNumbers(List<Individual> individuals) {
		List<Double> fitnesses = new ArrayList<Double>();
		for (Individual individual : individuals) {
			fitnesses.add(calculateFitnessFromGenesAsBinaryNumber(individual));
		}
		return fitnesses;
	}

	private Double calculateFitnessFromGenesAsBinaryNumber(Individual individual) {
		long binaryVal = Long.valueOf(individual.getObjectsAsString(), 2);
		return Double.valueOf(binaryVal);

	}

	private String numberToBinaryStringWithLength(int number, int length) {
		String numberWithoutPadding = Integer.toBinaryString(number);
		StringBuilder builder = new StringBuilder();
		builder.append(numberWithoutPadding);
		while (builder.length() < length) {
			builder.insert(0, '0');
		}
		return builder.toString();
	}

	@Test
	public void maxFitnessShouldNotDecreaseWithIterationsWhenElitismIsTrue() {
		EvolutionProcessor processor = new EvolutionProcessor(0.5, 0.2, true);
		List<Individual> population = new ArrayList<Individual>();
		for (int i = 1; i <= 256; i = i * 2) {
			population.add(new Individual(numberToBinaryStringWithLength(i, 10)));
		}
		List<Double> fitnesses = calculateFitnessesFromGenesAsBinaryNumbers(population);
		double maxFitness = Collections.max(fitnesses);
		logger.info("Population at the beginning: " + population);
		logger.info("Max fitness at the beginning: " + maxFitness);
		for (int i = 0; i < 10; ++i) {
			for (int j = 0; j < 5; ++j) {
				population = processor.getIndividualsForNextIteration(population, fitnesses);
				fitnesses = calculateFitnessesFromGenesAsBinaryNumbers(population);
			}
			double oldMaxFitness = maxFitness;
			maxFitness = Collections.max(fitnesses);
			logger.info("Max fitness after " + (i + 1) * 5 + " iterations: " + maxFitness);
			Assert.assertTrue(maxFitness >= oldMaxFitness);
		}

		logger.info("Max fitness at the end: " + maxFitness);
		logger.info("Population at the end: " + population);
	}
}
