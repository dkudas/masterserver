

function makeRequest(url, data){
  var mHttpReq = new XMLHttpRequest();
  mHttpReq.open("GET", url, false);
  mHttpReq.send(data);
  response = mHttpReq.responseText;
  return response;
}

function loadKernel() {
  var kernelSource = makeRequest('kernel.js', null);
  return kernelSource;
}

function loadData() {
  var data = makeRequest('getTask', null);
  var json = JSON.parse(data);
  return json;
}

function sendResult(url, result) {
  var dataToSend = JSON.stringify(result);
  var mHttpReq = new XMLHttpRequest();
  mHttpReq.open("POST", 'sendResult');
  mHttpReq.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
  mHttpReq.send(dataToSend);
  response = mHttpReq.responseText;
  return response;
}

function vectorAdd () {

  // All output is written to element by id "output"
  var result = document.getElementById("result");
  result.innerHTML = '<div class="alert alert-info"><strong>Current status: </strong><span id="output">Calculations not started yet.</span></div>'
  var output = document.getElementById("output");
 
  try {
  
    // First check if the WebCL extension is installed at all 
    if (window.webcl == undefined) {
      alert("Unfortunately your system does not support WebCL. " +
            "Make sure that you have both the OpenCL driver " +
            "and the WebCL browser extension installed.");
      return false;
    }

    // Generate input vectors
    var kernelSrc = loadKernel("clProgramVectorAdd");
    while(1) {
        var data = loadData("url");
        if(data.hasOwnProperty("msg")) {
            output.innerHTML = data.msg;
            setTimeout(vectorAdd, 1000);
            break;
        }
        var requestId = data.id;
        var numberOfIndividuals = 0;
        for(var i=0; i<data.countingIslandEntities.length; i++) {
            numberOfIndividuals += data.countingIslandEntities[i].individuals.length;
        }
        var creatureSize = data.masses.length;
        var vectorLength = numberOfIndividuals*creatureSize;
        var UIvector1 = new Uint32Array(vectorLength);    
        var UIvector2 = new Float32Array(creatureSize*2);
        var creature_counter = 0;
        for ( var k=0; k<data.countingIslandEntities.length;  k++) {
        for ( var i=0; i<data.countingIslandEntities[k].individuals.length;  i++) {
          for( var j=0; j<creatureSize; j++) {
            if(data.countingIslandEntities[k].individuals[i].objects[j]) {
              UIvector1[i*creatureSize+j] = 1;
            } else {
              UIvector1[i*creatureSize+j] = 0;
	   }
          }
          creature_counter++;
        }
        for ( var i=0; i<creatureSize;  i++) {
          UIvector2[i*2] = data.values[i];
          UIvector2[i*2+1] = data.masses[i];
        }
        var localSize = new Uint32Array(1);
        localSize[0] = vectorLength*8;    
        output.innerHTML = "Data initialized";

    // Setup WebCL context using the default device
    var ctx = webcl.createContext ();
                     
    // Reserve buffers
    var bufSize = vectorLength * 4; // size in bytes
    //output.innerHTML += "<br>Buffer size: " + bufSize + " bytes";
    var bufIn1 = ctx.createBuffer (WebCL.MEM_READ_ONLY, bufSize);
    var bufIn2 = ctx.createBuffer (WebCL.MEM_READ_ONLY, creatureSize*8);
    var bufOut = ctx.createBuffer (WebCL.MEM_WRITE_ONLY, vectorLength/creatureSize*4);
    output.innerHTML = "Buffers created";
    // Create and build program for the first device
    output.innerHTML = "Got kernel";
    var program = ctx.createProgram(kernelSrc);
    var device = ctx.getInfo(WebCL.CONTEXT_DEVICES)[0];

    try {
      program.build ([device], "");
    } catch(e) {
      alert ("Failed to build WebCL program. Error "
             + program.getBuildInfo (device, 
                                            WebCL.PROGRAM_BUILD_STATUS)
             + ":  " 
             + program.getBuildInfo (device, 
                                            WebCL.PROGRAM_BUILD_LOG));
      throw e;
    }
    output.innerHTML = "Program builded";
    // Create kernel and set arguments
    var kernel = program.createKernel ("ckVectorAdd");
    kernel.setArg (0, bufIn1);
    kernel.setArg (1, bufIn2);
    kernel.setArg (2, new Uint32Array([creatureSize*4*2]));    
    kernel.setArg (3, bufOut);
    kernel.setArg (4, new Uint32Array([vectorLength]));
    kernel.setArg (5, new Float32Array([data.capacity]));
    output.innerHTML = "Arguments set and kernel created";
   // Create command queue using the first available device
    var cmdQueue = ctx.createCommandQueue (device);
    
    // Write the buffer to OpenCL device memory
    cmdQueue.enqueueWriteBuffer (bufIn1, false, 0, bufSize, UIvector1);
    cmdQueue.enqueueWriteBuffer (bufIn2, false, 0, creatureSize*8, UIvector2);
 
    // Init ND-range
    var localWS = [creatureSize,2];
    var globalWS = [vectorLength,2];

    //output.innerHTML += "<br>Global work item size: " + globalWS;
    //output.innerHTML += "<br>Local work item size: " + localWS;
    
    // Execute (enqueue) kernel
    output.innerHTML = "Executing...";
    
    cmdQueue.enqueueNDRangeKernel(kernel, globalWS.length, null, 
                                  globalWS, localWS);

    // Read the result buffer from OpenCL device
    var outBuffer = new Float32Array(vectorLength/creatureSize);
    cmdQueue.enqueueReadBuffer (bufOut, false, 0, vectorLength/creatureSize*4, outBuffer);    
    cmdQueue.finish (); //Finish all the operations

    //Print input vectors and result vector
    
    result.innerHTML = '<div class="alert alert-success"><strong>Current status: </strong><span id="output"></span></div>'
    output = document.getElementById("output");
    output.innerHTML += "Result = ";
    for (var i = 0; i < vectorLength/creatureSize; i = i + 1) {
      output.innerHTML += outBuffer[i] + ", ";
    }
    var result = {"id" : requestId, "countingIslandResults" : []}
    var iter = 0;
    var calcRes = []
    for(var i = 0; i<data.countingIslandEntities.length; i++) {
        for(var j = 0; j<data.countingIslandEntities[i].individuals.length; j++) {
            calcRes.push(outBuffer[iter]);
            iter++;
        }
        var islandResult = { "id" : data.countingIslandEntities[i].id, "fitnesses" : calcRes }
        result.countingIslandResults.push(islandResult);
    }
    if(Math.random()<0.1) {
        console.log("Aborted.");
        ctx.releaseAll();
        continue;
    }
    var response = sendResult('url', result);
    output.innerHTML += " result sent";
    ctx.releaseAll();
  }
}
  } catch(e) {
    document.getElementById("output").innerHTML 
      += "<h3>ERROR:</h3><pre style=\"color:red;\">" + e.message + "</pre>";
    throw e;
  }
}
