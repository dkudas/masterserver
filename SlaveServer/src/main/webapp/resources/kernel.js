kernel void ckVectorAdd(global uint* vectorIn1, 
                          global float* vectorIn2,
                          local float* scratch,
                          global float* vectorOut,
                          const uint uiVectorWidth,
                          const float capacity) {
     uint x = get_global_id(0);
     uint y = get_global_id(1);
     uint local_x = get_local_id(0);
     uint local_y = get_local_id(1);
     uint work_group = get_group_id(0);
     if (x >= uiVectorWidth)
     {
       return;
    }
    // add the vector elements
    scratch[local_x*2+local_y] = vectorIn1[x] * vectorIn2[local_x*2+local_y];
    barrier(CLK_LOCAL_MEM_FENCE);
    for(int offset = get_local_size(0)/2; offset > 0; offset >>= 1) {
        if (local_x < offset) {
            float other = scratch[(local_x+offset)*2+local_y];
            float mine = scratch[local_x*2+local_y];
            scratch[local_x*2+local_y] = other + mine;
        }
        barrier(CLK_LOCAL_MEM_FENCE);
    }
    if(local_x == 0) {
        if(scratch[1]<capacity) {
            vectorOut[work_group] = scratch[0]; 
        } else {
            vectorOut[work_group] = 0;
        }  
    }
  }
