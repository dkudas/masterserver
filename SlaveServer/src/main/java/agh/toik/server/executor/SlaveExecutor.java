package agh.toik.server.executor;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.CountingIslandResult;
import agh.toik.common.communication.CountingEntity;
import agh.toik.common.communication.CountingResult;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Set;

/**
 * Created by DAMIAN on 19.05.14.
 */

@Component
public class SlaveExecutor {

    public static final String TASK_FOR_SLAVE="/tasksForSlave";

    private static final Logger logger = Logger.getLogger(SlaveExecutor.class);
    private volatile boolean done;
    private String address_master;
    private CountingEntity countingEntity;
    private long timeval= 10000;
    private Set<Long> idsIslands= new HashSet<Long>();
    private static CountingResult mainCountingResult;

    public void loadProperties(){
        Resource resource = new ClassPathResource("ip.properties");
        Properties props = new Properties();
        try {
            props.load(resource.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        address_master= props.getProperty("addressMaster");
    }

    @PostConstruct
    public void execute() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                logger.info("Start slave execution");
                loadProperties();

                boolean checking= true;
                while(checking){
                    RestTemplate restTemplate = new RestTemplate();
                    String mess = restTemplate.getForObject(address_master+TASK_FOR_SLAVE, String.class);
                    System.out.println(mess);
                    countingEntity= CountingEntity.fromJson(mess);
                    System.out.println(countingEntity.getCountingIslandEntities().size());
                    if(!countingEntity.getCountingIslandEntities().isEmpty()) {
                        idsIslands= new HashSet<Long>();
                        mainCountingResult= new CountingResult(countingEntity.getId(),new LinkedList<CountingIslandResult>());
                        for(CountingIslandEntity c: countingEntity.getCountingIslandEntities())
                            idsIslands.add(c.getId());
                        logger.debug("Size of idsIslands: " + idsIslands.size());
                        checking = false;
                    }
                    else
                        try {
                            Thread.sleep(timeval);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                }
                done= true;

            }
        }).start();
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done){
        this.done=done;
    }

    public CountingEntity getCountingEntity() {
        return countingEntity;
    }

    public void setCountingEntity(CountingEntity countingEntity) {
        this.countingEntity = countingEntity;
    }

    public String getAddress_master() {
        return address_master;
    }

    public void setAddress_master(String address_master) {
        this.address_master = address_master;
    }

    public Set<Long> getIdsIslands() {
        return idsIslands;
    }

    public void setIdsIslands(Set<Long> idsIslands) {
        this.idsIslands = idsIslands;
    }

    public static CountingResult getMainCountingResult() {
        return mainCountingResult;
    }

    public static void setMainCountingResult(CountingResult mainCountingResult) {
        SlaveExecutor.mainCountingResult = mainCountingResult;
    }
}
