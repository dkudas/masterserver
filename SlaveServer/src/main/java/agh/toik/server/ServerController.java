package agh.toik.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.communication.CountingEntity;
import agh.toik.common.communication.CountingResult;
import agh.toik.server.executor.SlaveExecutor;

/**
 * Created by DAMIAN on 02.05.14.
 */
@Controller
public class ServerController {
    private static final Logger logger = Logger.getLogger(ServerController.class);
    private static final String MSG_THX = "{\"msg\": \"Thx\"}";
    private static final String MSG_NO_TASK = "{\"msg\": \"No task\"}";
    public static final String RESULTS_FROM_SLAVE = "/resultsFromSlave";
    public static final String CLIENT_KERNEL = "http://wreszelewski.pl/toik-client/kernel.js";

    @Autowired
    private SlaveExecutor slaveExecutor;

    private CountingEntity countingEntity = null;
    private List<CountingIslandEntity> islandEntities = null;

    private final int TIMEOUT_IN_SECONDS = 60;
    //private static int nr_task = 0;
    private long currentId;

    enum CountingIslandState {
        NEW, SENT_TO_VOLUNTEER, FINISHED
    }

    private Map<Integer, CountingIslandState> islandStates = new HashMap<Integer, CountingIslandState>();
    private Map<Integer, Long> islandSentTimes = new HashMap<Integer, Long>();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home() {
        return "index.html";
    }

    private void markIslandWithGivenIdAsFinished(long islandId) {
        for (int i = 0; i < islandEntities.size(); ++i) {
            CountingIslandEntity islandEntity = islandEntities.get(i);
            if (islandEntity.getId() == islandId) {
                islandStates.put(i, CountingIslandState.FINISHED);
                break;
            }
        }
    }

    private boolean areAllIslandsFinished() {
        for (CountingIslandState state : islandStates.values()) {
            if (state != CountingIslandState.FINISHED) {
                return false;
            }
        }
        return true;
    }

    private void cleanCountingStates() {
        this.slaveExecutor.setDone(false);
        this.countingEntity = null;
        this.islandEntities = null;
        this.islandStates.clear();
        this.islandSentTimes.clear();
    }

    private void initializeCountingStates() {
        this.countingEntity = slaveExecutor.getCountingEntity();
        currentId=countingEntity.getId();
        this.islandEntities = countingEntity.getCountingIslandEntities();
        for (int i = 0; i < islandEntities.size(); ++i) {
            islandStates.put(i, CountingIslandState.NEW);
        }
    }

    private CountingIslandEntity getIslandToCount() {
        for (int i = 0; i < islandEntities.size(); ++i) {
            CountingIslandEntity island = islandEntities.get(i);
            CountingIslandState islandState = islandStates.get(i);
            if (islandState == CountingIslandState.NEW) {
                islandSentTimes.put(i, System.currentTimeMillis());
                islandStates.put(i, CountingIslandState.SENT_TO_VOLUNTEER);
                return island;

            } else if (islandState == CountingIslandState.SENT_TO_VOLUNTEER) {
                long differenceInMillis = System.currentTimeMillis() - islandSentTimes.get(i);
                long differenceInSeconds = differenceInMillis / 1000;
                if (differenceInSeconds > TIMEOUT_IN_SECONDS) {
                    islandSentTimes.put(i, System.currentTimeMillis());
                    logger.debug("Timeout for island " + i);
                    return island;
                }
            }
        }
        return null;
    }

    @RequestMapping(value = "getTask", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getTask() {

        if(!slaveExecutor.isDone())
            return MSG_NO_TASK;

        if (countingEntity == null) {
            initializeCountingStates();
        }
        CountingIslandEntity islandToCount = getIslandToCount();
        if (islandToCount != null) {
            List<CountingIslandEntity> countingIslandEntities = new LinkedList<CountingIslandEntity>();
            countingIslandEntities.add(islandToCount);

            CountingEntity cE = new CountingEntity(countingEntity.getId(), countingEntity.getCapacity(),
                    countingIslandEntities, countingEntity.getMasses(), countingEntity.getValues());
            return cE.toJson();
        }

        return MSG_NO_TASK;
    }

    @RequestMapping(value = "getCode", method = RequestMethod.GET)
    @ResponseBody
    public String getCode() {

        StringBuilder builder = new StringBuilder("");
        try {
            URL url = new URL(CLIENT_KERNEL);

            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                builder.append(inputLine);
            in.close();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    @RequestMapping(value = "sendResult", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String sendResult(@RequestBody String countingJson) {

        if (currentId != this.countingEntity.getId()){
        //    System.out.println("NIE TEN id");
            return MSG_THX;
        }
        System.out.println(countingJson);
        System.out.println("SIEMA");
        CountingResult countingResult = CountingResult.fromJson(countingJson);
        RestTemplate restTemplate = new RestTemplate();
        Long id = countingResult.getCountingIslandResults().get(0).getId();

        if (slaveExecutor.getIdsIslands().contains(id)) {
            markIslandWithGivenIdAsFinished(id);
            slaveExecutor.getIdsIslands().remove(id);
            System.out.println("Size " + slaveExecutor.getIdsIslands().size());
            slaveExecutor.getMainCountingResult().addCountingIslandResult(
                    countingResult.getCountingIslandResults().get(0));
        }

        if (areAllIslandsFinished()) {
            String mess = slaveExecutor.getMainCountingResult().toJson();
            restTemplate.postForLocation(slaveExecutor.getAddress_master() + RESULTS_FROM_SLAVE, mess);
            //++nr_task;
            cleanCountingStates();
            slaveExecutor.execute();
        }

        return MSG_THX;
    }
}
