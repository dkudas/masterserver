package agh.toik.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

/**
 * Created by Adiki on 2014-05-17.
 */
public class CountingIslandEntity {
    private long id;
    private List<Individual> individuals;

    public CountingIslandEntity(long id, List<Individual> individuals) {
        this.id = id;
        this.individuals = individuals;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public long getId() {
        return id;
    }

    public List<Individual> getIndividuals() {
        return individuals;
    }
}
