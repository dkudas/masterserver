package agh.toik.common;

import java.util.Arrays;

/**
 * Represents an individual for an evolutionary algorithm of solving a knapsack problem.
 * 
 */
public class Individual {

	@Override
	public String toString() {
		return "Individual [objects=" + getObjectsAsString() + "]";
	}

	private boolean[] objects;

	public Individual(boolean[] objects) {
		this.objects = objects;
	}

	public Individual(String objectsAsString) {
		objects = new boolean[objectsAsString.length()];
		for (int i = 0; i < objectsAsString.length(); ++i) {
			char c = objectsAsString.charAt(i);
			objects[i] = (c == '1');
		}

	}

	public Individual(int length) {
		this.objects = new boolean[length];
	}

	public String getObjectsAsString() {
		StringBuilder builder = new StringBuilder();
		for (boolean object : objects) {
			builder.append(object ? "1" : "0");
		}
		return builder.toString();
	}

	public boolean[] getObjects() {
		return objects.clone();
	}

	public boolean getObject(int position) {
		return objects[position];
	}

	public int getLength() {
		return objects.length;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(objects);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Individual other = (Individual) obj;
		if (!Arrays.equals(objects, other.objects))
			return false;
		return true;
	}

}
