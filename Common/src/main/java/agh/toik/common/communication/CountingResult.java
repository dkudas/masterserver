package agh.toik.common.communication;

import agh.toik.common.CountingIslandResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Adiki on 2014-05-16.
 */
public class CountingResult {
    private long id;
    private List<CountingIslandResult> countingIslandResults;

    public CountingResult(long id, List<CountingIslandResult> countingIslandResults) {
        this.id = id;
        this.countingIslandResults = countingIslandResults;
    }

    public static CountingResult fromJson(String json) {
        Gson gson = new Gson();
        CountingResult result= gson.fromJson(json, CountingResult.class);
        return result;
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<CountingIslandResult> getCountingIslandResults() {
        return countingIslandResults;
    }

    public void setCountingIslandResults(List<CountingIslandResult> countingIslandResults) {
        this.countingIslandResults = countingIslandResults;
    }

    public void addCountingIslandResult(CountingIslandResult countingIslandResult){
        this.countingIslandResults.add(countingIslandResult);
    }

    public static void main(String[] args) {
        List<CountingIslandResult> countingIslandResults = new ArrayList<CountingIslandResult>();

        countingIslandResults.add(new CountingIslandResult(1, Arrays.asList(1.0, 2.0, 3.0)));
        countingIslandResults.add(new CountingIslandResult(2, Arrays.asList(1.0, 2.0, 3.0)));
        CountingResult countingResult = new CountingResult(12342345234534L, countingIslandResults);
        System.out.println(countingResult.toJson());

    }

}
