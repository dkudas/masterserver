package agh.toik.common.communication;

import agh.toik.common.CountingIslandEntity;
import agh.toik.common.Individual;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Adiki on 2014-05-16.
 */
public class CountingEntity {

    private long id;
    private double capacity;
    private List<CountingIslandEntity> countingIslandEntities;
    private List<Double> masses;
    private List<Double> values;

    public CountingEntity(long id, double capacity, List<CountingIslandEntity> countingIslandEntities, List<Double> masses, List<Double> values) {
        this.id = id;
        this.capacity = capacity;
        this.countingIslandEntities = countingIslandEntities;
        this.masses = masses;
        this.values = values;
    }

    public static CountingEntity fromJson(String json){
        Gson gson= new Gson();
        return gson.fromJson(json, CountingEntity.class);
    }

    public String toJson() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(this);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getCapacity() {
        return capacity;
    }

    public void setCapacity(double capacity) {
        this.capacity = capacity;
    }

    public List<CountingIslandEntity> getCountingIslandEntities() {
        return countingIslandEntities;
    }

    public void setCountingIslandEntities(List<CountingIslandEntity> countingIslandEntities) {
        this.countingIslandEntities = countingIslandEntities;
    }

    public List<Double> getMasses() {
        return masses;
    }

    public void setMasses(List<Double> masses) {
        this.masses = masses;
    }

    public List<Double> getValues() {
        return values;
    }

    public void setValues(List<Double> values) {
        this.values = values;
    }

}
