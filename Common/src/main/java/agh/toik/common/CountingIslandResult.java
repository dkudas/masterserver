package agh.toik.common;

import java.util.List;

/**
 * Created by Adiki on 2014-05-17.
 */
public class CountingIslandResult {
    private long id;
    private List<Double> fitnesses;

    public CountingIslandResult(long id, List<Double> fitnesses) {
        this.id = id;
        this.fitnesses = fitnesses;
    }

    public long getId() {
        return id;
    }

    public List<Double> getFitnesses() {
        return fitnesses;
    }
}
